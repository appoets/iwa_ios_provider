//
//  SignUpControllers.swift
//  GoJekUser
//
//  Created by Santhosh on 02/09/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import UIKit

class NewLoginViewController : UIViewController{
    @IBOutlet var iconView: UIView!
    @IBOutlet weak var mobileText: UITextField!
    
//    @IBOutlet weak var iconView: UIView!
//    @IBOutlet weak var stckImg1View: UIView!
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var subView: UIView!
    @IBOutlet var stckImg1View: UIView!
//    @IBOutlet weak var stckImg2View: UIView!
    @IBOutlet var stckImg2View: UIView!
    
    @IBOutlet var stckImg3View: UIView!
    //    @IBOutlet weak var stckImg3View: UIView!
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLoad() {
        self.view.applyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
        self.continueBtn.applyGradientBtn(isVertical: true, colorArray: [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
        self.navigationController?.navigationBar.isHidden = true
        initialLoads()
        
        
    }
    func initialLoads(){
        setCornerradius()
        self.continueBtn.setTitle(Constant.Continue, for: .normal)
        continueBtn.addTarget(self, action: #selector(continueBtnAction), for: .touchUpInside)
        
        
    }
    
    func setCornerradius(){
        self.subView.setCornerRadiuswithValue(value: 22)
        self.stckImg1View.setCornerRadius()
        self.stckImg2View.setCornerRadius()
        self.stckImg3View.setCornerRadius()
        iconView.layer.cornerRadius = 17
        self.continueBtn.setCornerRadiuswithValue(value: 22)
        self.mobileText.setBorder(width: 1, color: .black)
        self.mobileText.setCornerRadiuswithValue(value: 23)
       
    }

}

extension NewLoginViewController{
    
    @objc func continueBtnAction(){
        
        let nextVc = storyboard?.instantiateViewController(withIdentifier: "OtpViewController") as! OtpViewController
        navigationController?.pushViewController(nextVc, animated: true)
        
    }
}

   


extension UIView {

    func applyGradient(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
         
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    

}
extension UIButton {

    func applyGradientBtn(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
         
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    

}


