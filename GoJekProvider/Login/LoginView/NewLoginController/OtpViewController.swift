//
//  SignUpControllers.swift
//  GoJekUser
//
//  Created by Santhosh on 02/09/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import Foundation
import UIKit

class OtpViewController : UIViewController{
    
    //    @IBOutlet var iconView: UIView!
    
    @IBOutlet weak var otp3: UITextField!
    @IBOutlet weak var otpTitleLbl: UILabel!
    @IBOutlet weak var resendBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var continueBbtn: UIButton!
    @IBOutlet weak var otp4: UITextField!
    @IBOutlet weak var otp2: UITextField!
    @IBOutlet weak var otp1: UITextField!
    @IBOutlet weak var stckImg1View: UIView!
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var subVieaw: UIView!
    @IBOutlet weak var stckImg3View: UIView!
    @IBOutlet weak var stckImg2View: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.aplyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
        self.continueBbtn.applyGradientBtn(isVertical: true, colorArray:  [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
    }
    
    override func viewDidLoad() {
//        iconView.setCornerRadius()
        self.navigationController?.navigationBar.isHidden = true
        initialLoads()
        self.continueBbtn.setTitle(Constant.Continue, for: .normal)
        self.continueBbtn.setTitle(Constant.ResendOtp, for: .normal)
       
        continueBbtn.addTarget(self, action: #selector(continueBbtnAction), for: .touchUpInside)
        backBtn.addTarget(self, action: #selector(backBtnAction), for: .touchUpInside)
        
        
       
//
    }
    func initialLoads(){
        
        setCornerradius()
        setOtpDesign()
    }
   
    func setCornerradius(){
        self.iconView.setCornerRadiuswithValue(value: 15)
        self.subVieaw.setCornerRadiuswithValue(value: 22)
        self.stckImg1View.setCornerRadius()
        self.stckImg2View.setCornerRadius()
        self.stckImg3View.setCornerRadius()
        self.continueBbtn.setCornerRadiuswithValue(value: 23)
        

   
    }
    func setOtpDesign(){
        
        self.otp1.setCornerRadiuswithValue(value: 15)
        self.otp2.setCornerRadiuswithValue(value: 15)
        self.otp3.setCornerRadiuswithValue(value: 15)
        self.otp4.setCornerRadiuswithValue(value: 15)
        self.otp1.setBorder(width: 1, color: .black)
        self.otp2.setBorder(width: 1, color: .black)
        self.otp3.setBorder(width: 1, color: .black)
        self.otp4.setBorder(width: 1, color: .black)
    }
    

}
extension OtpViewController{
    @objc func backBtnAction(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func continueBbtnAction(){
        
    }
}


   


extension UIView {

    func aplyGradient(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
         
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }

}
extension UIButton {

    func applyGradientBtn1(isVertical: Bool, colorArray: [UIColor]) {
        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
         
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colorArray.map({ $0.cgColor })
        if isVertical {
            //top to bottom
            gradientLayer.locations = [0.0, 1.0]
        } else {
            //left to right
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        }
        
        backgroundColor = .clear
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
    

}


