//
//  LoginView.swift
//  GoJekProvider
//
//  Created by ABIRAMI R on 07/09/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class LoginView: UIView {

    @IBOutlet weak var checkboxBtn1: UIButton!
    @IBOutlet weak var mobileTextField: CustomTextField!
    @IBOutlet weak var countrycodeTxtfield: CustomTextField!
    @IBOutlet weak var textfieldMainView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var signInBtn: UIButton!
    
    @IBOutlet weak var termsConditionLbl: UILabel!
    @IBOutlet weak var checkBoxbtn2: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.signInBtn.applyGradient(isVertical: true, colorArray: [#colorLiteral(red: 0.1960784314, green: 0.8235294118, blue: 0.7921568627, alpha: 1), #colorLiteral(red: 0.4588235294, green: 0.4196078431, blue: 0.9450980392, alpha: 1)])
        initialLoads()
        setText()
    }
    
}
extension LoginView {
    
    func initialLoads(){
        self.checkBoxbtn2.addTarget(self, action: #selector(checkBoxbtn2Action), for: .touchUpInside)
        self.checkboxBtn1.addTarget(self, action: #selector(checkboxBtn1Action), for: .touchUpInside)
        self.signInBtn.addTarget(self, action: #selector(countinueAction), for: .touchUpInside)
        self.mainView.setOneSideCorner(corners: [.topRight,.topLeft], radius: 10)
    }
    
    func setText(){
        self.TitleLbl.text = Constant.IWA.localized
        self.signInBtn.setTitle(Constant.Continue.localized, for: .normal)
        self.signInBtn.backgroundColor = UIColor(red: 104/255, green: 123/255, blue: 229/255, alpha: 1)
    }
    func setClour(){
        
    }
    @objc func checkBoxbtn2Action(){
        self.checkboxBtn1.setImage(UIImage(named: "ic_check"), for: .normal)
    }
    @objc func checkboxBtn1Action(){
        self.checkBoxbtn2.setImage(UIImage(named: "ic_check"), for: .normal)
    }
    
    @objc func countinueAction(){
        
    }
}
//extension UIButton {
//
//    func applyGradient(isVertical: Bool, colorArray: [UIColor]) {
//        layer.sublayers?.filter({ $0 is CAGradientLayer }).forEach({ $0.removeFromSuperlayer() })
//
//        let gradientLayer = CAGradientLayer()
//        gradientLayer.colors = colorArray.map({ $0.cgColor })
//        if isVertical {
//            //top to bottom
//            gradientLayer.locations = [0.0, 1.0]
//        } else {
//            //left to right
//            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.4)
//            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
//        }
//
//        backgroundColor = .clear
//        gradientLayer.frame = bounds
//        layer.insertSublayer(gradientLayer, at: 0)
//    }
//
//}
